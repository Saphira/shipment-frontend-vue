FROM node:10.19.0-alpine3.11 AS builder

COPY package*.json /app/

WORKDIR /app
RUN npm update & \
    npm ci

COPY . /app
RUN npm run build

FROM nginx:1.16.1-alpine
RUN apk add --no-cache bash
COPY --from=builder /app/dist /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY ./conf/nginx.conf /etc/nginx/conf.d/default.conf

WORKDIR /usr/share/nginx/html
COPY ./conf/set_env.sh .
COPY .env .
RUN chmod +x set_env.sh


CMD ["/bin/bash", "-c", "/usr/share/nginx/html/set_env.sh && nginx -g \"daemon off;\""]

