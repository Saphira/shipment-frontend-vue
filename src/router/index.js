import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import ShipmentList from '@/components/ShipmentList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/shipment-list',
      name: 'ShipmentList',
      component: ShipmentList
    },
 ]
})