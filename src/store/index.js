import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const apiUrl = window._env_ && window._env_.API_URL
? window._env_.API_URL
:'http://localhost:8000/api/v1';

export default new Vuex.Store({
  state: {
    shipments: [],
    system: {
      error: {
        messages: [],
        show: false
      }
    }
  },
  actions: {
    LOAD_SHIPMENT: ({commit}) => {
      axios.get(`${apiUrl}/shipments/`)
      .then(response => {
        commit('SET_SHIPMENT', {shipments: response.data.results})
      })
      .catch(error => {
        commit('SHOW_ERROR', { errors: error.config.data })
      })
    },
    SAVE_SHIPMENT: ({ commit }, { shipment }) => {
      return axios.post(`${apiUrl}/shipments/`, shipment)
        .then(response => {
          commit('ADD_SHIPMENT', { shipment: response.data })

          return response.data
        })
        .catch(error => {
          commit('SHOW_ERROR', { errors: error.response.data })
        })
    },
    UPDATE_SHIPMENT: ({commit}, {shipment, index}) => {  
      return axios.put(`${apiUrl}/shipments/${shipment.id}/`, shipment)
        .then(response => {
          commit('UPDATE_SHIPMENT', { shipment: response.data, index })

          return response.data
        })
        .catch(error => {
          commit('SHOW_ERROR', { errors: error.response.data })
        })
    },
    DELETE_SHIPMENT: ({commit}, {shipment, index} ) => { 

      return axios.delete(`${apiUrl}/shipments/${shipment.id}/`, shipment)
        .then(response => {
          commit('DELETE_SHIPMENT', { index })

          return response.data
        })
        .catch(error => {
          commit('SHOW_ERROR', {  errors: error.response.data })
        })
    },
  },
  mutations: {
    SET_SHIPMENT: (state, {shipments}) => {
      state.shipments = shipments.map(shipment =>  {
        shipment.delivery_date = new Date(shipment.delivery_date)
        shipment.created_at = new Date(shipment.created_at)
        return shipment
      })
    },
    ADD_SHIPMENT: (state, { shipment }) => {
      shipment.delivery_date = new Date(shipment.delivery_date)
      shipment.created_at = new Date(shipment.created_at)
      state.shipments.push(shipment)
    },
    UPDATE_SHIPMENT: (state, {shipment, index})=> {
      shipment.delivery_date = new Date(shipment.delivery_date)
      shipment.created_at = new Date(shipment.created_at)
      Object.assign(state.shipments[index], shipment)
    },
    DELETE_SHIPMENT: (state, {index})=> {
      let shipments = state.shipments
      shipments.splice(index,1)
      Object.assign(state.shipments, shipments)
    },
    SHOW_ERROR: (state, { errors }) => {
      state.system.error.show = true
      state.system.error.messages = Object.keys(errors).map(key => `${key}: ${errors[key][0]}`)

      setTimeout(() => {
        state.system.error.show = false
        state.system.error.messages = []
      }, 5000)
    }
  },
  getters: {
    getShipments: state => {
      return state.shipments
    },
    getPagination: state => {
      return state.pagination
    },
    getSystem: state => {
      return state.system
    }
  }
})

