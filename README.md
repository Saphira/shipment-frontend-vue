# Shipment Management Frontend

This is a test application with Vue.js as Frontend for a simple Shipment list


## Includes

* Vue CLI
* Vue Router
* Vuex
* Vuetify
* Axios
* Cypress


## Prerequisites

Before getting started you should have the following installed and running:
- [X] Node v10.15.3- [instructions](https://nodejs.dev/learn/how-to-install-nodejs)
- [X] Vue CLI - [instructions](https://cli.vuejs.org/guide/installation.html)

## Project setup
```
npm install
```

## Compiles and hot-reloads for development
```
npm run serve
```

## Run cypress tests 
npx cypress run

# Build application with docker
* Build docker image: `docker build -t shipment-frontend:0.1.0`
* Run Frontend container: `docker run -d -e API_URL=http://[BACKEND_ADDRESS]:[BACKEND_PORT]/api/v1 -p 8586:80  shipment-frontend:0.1.0`
* Open your Browser in: http://localhost:8586