describe('Shipment UI Tests', () => {
    it('should allow to add a news shipment', () => {
      cy.visit('/shipment-list/')
      cy.get('[data-testid=btn-add]').click()
      cy.get('[data-testid=input-tracking-number]').type("0001")
      cy.get('[data-testid=input-location-origin]').type("Tallinn")
      cy.get('[data-testid=input-location-destination]').type("Tartu")
      cy.get('[data-testid=btn-submit]').click()
      cy.get('[data-testid=table-shipment]').eq(0).should('contain', 0001)
    })
  })